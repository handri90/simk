/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.24 : Database - sim_k
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sim_k` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sim_k`;

/*Table structure for table `indikator_sasaran` */

DROP TABLE IF EXISTS `indikator_sasaran`;

CREATE TABLE `indikator_sasaran` (
  `id_indikator_sasaran` bigint(30) NOT NULL AUTO_INCREMENT,
  `sasaran_id` bigint(30) DEFAULT NULL,
  `uraian_indikator` text,
  `satuan_id` int(11) DEFAULT NULL,
  `m_1` double(5,0) DEFAULT NULL,
  `m_2` double(5,0) DEFAULT NULL,
  `m_3` double(5,0) DEFAULT NULL,
  `m_4` double(5,0) DEFAULT NULL,
  `m_5` double(5,0) DEFAULT NULL,
  `m_6` double(5,0) DEFAULT NULL,
  `m_7` double(5,0) DEFAULT NULL,
  `m_8` double(5,0) DEFAULT NULL,
  `m_9` double(5,0) DEFAULT NULL,
  `m_10` double(5,0) DEFAULT NULL,
  `m_11` double(5,0) DEFAULT NULL,
  `m_12` double(5,0) DEFAULT NULL,
  `type_input` enum('0','1') DEFAULT NULL COMMENT '1 = perubahan, 0 = tidak',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_indikator_sasaran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `indikator_sasaran` */

insert  into `indikator_sasaran`(`id_indikator_sasaran`,`sasaran_id`,`uraian_indikator`,`satuan_id`,`m_1`,`m_2`,`m_3`,`m_4`,`m_5`,`m_6`,`m_7`,`m_8`,`m_9`,`m_10`,`m_11`,`m_12`,`type_input`,`created_at`,`updated_at`,`deleted_at`) values 
(3,1,'indikator satu dua',6,1,1,1,1,1,1,1,1,1,1,1,1,'0','2020-09-29 07:52:51','2020-09-29 08:25:14',NULL);

/*Table structure for table `level_user` */

DROP TABLE IF EXISTS `level_user`;

CREATE TABLE `level_user` (
  `id_level_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level_user` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_level_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `level_user` */

insert  into `level_user`(`id_level_user`,`nama_level_user`,`created_at`,`deleted_at`,`updated_at`) values 
(1,'superadmin','2020-01-15 07:42:14',NULL,NULL),
(2,'admin','2020-09-04 01:49:18',NULL,NULL);

/*Table structure for table `master_periode` */

DROP TABLE IF EXISTS `master_periode`;

CREATE TABLE `master_periode` (
  `id_periode` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_awal` char(4) DEFAULT NULL,
  `tahun_akhir` char(4) DEFAULT NULL,
  `periode_is_active` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_periode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `master_periode` */

insert  into `master_periode`(`id_periode`,`tahun_awal`,`tahun_akhir`,`periode_is_active`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'2018','2022','1','2020-09-10 08:20:14',NULL,NULL),
(2,'2022','2026','0','2020-09-10 08:20:17',NULL,NULL);

/*Table structure for table `master_sumber_anggaran` */

DROP TABLE IF EXISTS `master_sumber_anggaran`;

CREATE TABLE `master_sumber_anggaran` (
  `id_sumber_anggaran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sumber_anggaran` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_sumber_anggaran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `master_sumber_anggaran` */

insert  into `master_sumber_anggaran`(`id_sumber_anggaran`,`nama_sumber_anggaran`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'APBD','2020-09-28 09:51:17',NULL,NULL),
(2,'APBN','2020-09-28 09:51:22',NULL,NULL),
(3,'BLUD','2020-09-28 09:51:30',NULL,NULL);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(50) DEFAULT NULL,
  `id_parent_menu` int(11) DEFAULT NULL,
  `nama_module` varchar(100) DEFAULT NULL,
  `nama_class` varchar(100) DEFAULT NULL,
  `class_icon` varchar(20) DEFAULT NULL,
  `order_menu` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

insert  into `menu`(`id_menu`,`nama_menu`,`id_parent_menu`,`nama_module`,`nama_class`,`class_icon`,`order_menu`,`created_at`,`deleted_at`,`updated_at`) values 
(1,'User',0,'user','User','icon-user',2,'2020-01-14 10:16:58','2020-09-28 07:06:27','2020-01-26 11:44:32'),
(5,'Dashboard',0,'dashboard','Dashboard','icon-home4',1,'2020-01-14 14:06:31',NULL,NULL),
(6,'Level User',0,'level_user','Level_user','icon-accessibility',3,'2020-01-14 14:21:49','2020-09-28 07:06:11','2020-01-26 11:45:08'),
(7,'Menu',0,'menu','Menu','icon-menu4',4,'2020-01-19 13:40:22','2020-09-28 07:06:03','2020-01-26 11:47:37'),
(9,'Privilage Menu',0,'privilage_level','Privilage_level','icon-list',5,'2020-01-19 11:22:27','2020-09-28 07:06:07','2020-01-26 11:47:04'),
(18,'Sasaran',19,'sasaran/struktural','Sasaran/struktural','icon-menu3',1,'2020-09-10 08:04:08',NULL,NULL),
(19,'PK Struktural',0,'#','#','icon-menu3',6,'2020-09-24 08:24:01',NULL,'2020-09-24 08:25:15'),
(20,'Indikator',19,'indikator','Indikator','icon-menu3',2,'2020-09-24 08:27:15',NULL,NULL),
(21,'Program Kegiatan',19,'program_kegiatan/struktural','Program_kegiatan','icon-menu3',3,'2020-09-28 07:07:16',NULL,NULL),
(22,'PK Perubahan',0,'#','#','icon-menu3',7,'2020-09-28 10:30:43',NULL,NULL),
(23,'Sasaran',22,'sasaran/perubahan','Sasaran','icon-menu3',1,'2020-09-28 10:31:20',NULL,NULL),
(24,'Penilaian',19,'penilaian/struktural','Penilaian','icon-menu3',4,'2020-09-28 10:32:43',NULL,NULL),
(25,'Indikator',22,'indikator/perubahan','Indikator','icon-menu3',2,'2020-09-28 10:35:32',NULL,NULL),
(26,'Program Kegiatan',22,'program_kegiatan/perubahan','Program_kegiatan','icon-menu3',3,'2020-09-28 10:35:35',NULL,NULL),
(27,'Penilaian',22,'penilaian_struktural/perubahan','Penilaian_struktural','icon-menu3',4,'2020-09-28 10:36:08',NULL,NULL),
(28,'PK Fungsional',0,'#','#','icon-menu3',8,'2020-09-28 10:43:24',NULL,NULL),
(29,'Kegiatan',28,'perjanjian_fungsional','Perjanjian_fungsional','icon-menu3',1,'2020-09-28 10:44:08',NULL,NULL),
(30,'Penilaian',28,'penilaian_fungsional','Penilaian_fungsional','icon-menu3',2,'2020-09-28 10:44:41',NULL,NULL);

/*Table structure for table `penilaian_anggaran_struktural` */

DROP TABLE IF EXISTS `penilaian_anggaran_struktural`;

CREATE TABLE `penilaian_anggaran_struktural` (
  `id_penilaian_anggaran_struktural` bigint(30) NOT NULL AUTO_INCREMENT,
  `program_kegiatan_struktural_id` bigint(30) DEFAULT NULL,
  `n_bulan` int(2) DEFAULT NULL,
  `jumlah_anggaran_realisasi` double(15,0) DEFAULT NULL,
  `type_input` enum('0','1') DEFAULT NULL COMMENT '1 = perubahan, 0 = tidak',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penilaian_anggaran_struktural`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `penilaian_anggaran_struktural` */

insert  into `penilaian_anggaran_struktural`(`id_penilaian_anggaran_struktural`,`program_kegiatan_struktural_id`,`n_bulan`,`jumlah_anggaran_realisasi`,`type_input`,`created_at`,`updated_at`,`deleted_at`) values 
(1,3,9,1000000,'0','2020-09-30 09:37:05','2020-09-30 10:30:54',NULL);

/*Table structure for table `penilaian_indikator_sasaran` */

DROP TABLE IF EXISTS `penilaian_indikator_sasaran`;

CREATE TABLE `penilaian_indikator_sasaran` (
  `id_penilaian_indikator_sasaran` bigint(30) NOT NULL AUTO_INCREMENT,
  `indikator_sasaran_id` bigint(30) DEFAULT NULL,
  `n_bulan` int(2) DEFAULT NULL,
  `nilai_realisasi` double(5,0) DEFAULT NULL,
  `type_input` enum('0','1') DEFAULT NULL COMMENT '1 = perubahan, 0 = tidak',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penilaian_indikator_sasaran`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `penilaian_indikator_sasaran` */

insert  into `penilaian_indikator_sasaran`(`id_penilaian_indikator_sasaran`,`indikator_sasaran_id`,`n_bulan`,`nilai_realisasi`,`type_input`,`created_at`,`updated_at`,`deleted_at`) values 
(1,3,9,2,'0','2020-09-30 09:37:05','2020-09-30 10:30:54',NULL);

/*Table structure for table `privilage_level_menu` */

DROP TABLE IF EXISTS `privilage_level_menu`;

CREATE TABLE `privilage_level_menu` (
  `id_privilage` int(11) NOT NULL AUTO_INCREMENT,
  `level_user_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `create_content` enum('1','0') DEFAULT NULL,
  `update_content` enum('1','0') DEFAULT NULL,
  `delete_content` enum('1','0') DEFAULT NULL,
  `view_content` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_privilage`),
  KEY `fk_privilage_level_menu` (`level_user_id`),
  KEY `fk_privilage_menu_id` (`menu_id`),
  CONSTRAINT `fk_privilage_level_menu` FOREIGN KEY (`level_user_id`) REFERENCES `level_user` (`id_level_user`),
  CONSTRAINT `fk_privilage_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `privilage_level_menu` */

insert  into `privilage_level_menu`(`id_privilage`,`level_user_id`,`menu_id`,`create_content`,`update_content`,`delete_content`,`view_content`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,1,'1','1','1','1','2020-01-17 14:20:33','2020-09-08 01:08:59',NULL),
(2,1,5,'1','1','1','1','2020-01-17 14:20:54','2020-09-08 01:08:59',NULL),
(4,1,6,'1','1','1','1','2020-01-17 14:33:15','2020-09-08 01:08:59','2020-09-04 02:02:23'),
(5,1,7,'1','1','1','1','2020-01-19 13:41:58','2020-09-08 01:08:59',NULL),
(8,1,6,'1','1','1','1',NULL,'2020-09-08 01:08:59','2020-09-04 02:05:51'),
(9,1,6,'1','1','1','1',NULL,'2020-09-08 01:08:59','2020-09-04 02:09:02'),
(10,1,6,'1','1','1','1','2020-09-04 02:09:14','2020-09-08 01:08:59',NULL);

/*Table structure for table `program_kegiatan_fungsional` */

DROP TABLE IF EXISTS `program_kegiatan_fungsional`;

CREATE TABLE `program_kegiatan_fungsional` (
  `id_program_kegiatan_fungsional` bigint(15) NOT NULL AUTO_INCREMENT,
  `tahun` char(4) DEFAULT NULL,
  `pekerjaan_id` bigint(15) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `m_1` double(5,0) DEFAULT NULL,
  `m_2` double(5,0) DEFAULT NULL,
  `m_3` double(5,0) DEFAULT NULL,
  `m_4` double(5,0) DEFAULT NULL,
  `m_5` double(5,0) DEFAULT NULL,
  `m_6` double(5,0) DEFAULT NULL,
  `m_7` double(5,0) DEFAULT NULL,
  `m_8` double(5,0) DEFAULT NULL,
  `m_9` double(5,0) DEFAULT NULL,
  `m_10` double(5,0) DEFAULT NULL,
  `m_11` double(5,0) DEFAULT NULL,
  `m_12` double(5,0) DEFAULT NULL,
  `r_1` double(5,0) DEFAULT NULL,
  `r_2` double(5,0) DEFAULT NULL,
  `r_3` double(5,0) DEFAULT NULL,
  `r_4` double(5,0) DEFAULT NULL,
  `r_5` double(5,0) DEFAULT NULL,
  `r_6` double(5,0) DEFAULT NULL,
  `r_7` double(5,0) DEFAULT NULL,
  `r_8` double(5,0) DEFAULT NULL,
  `r_9` double(5,0) DEFAULT NULL,
  `r_10` double(5,0) DEFAULT NULL,
  `r_11` double(5,0) DEFAULT NULL,
  `r_12` double(5,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_program_kegiatan_fungsional`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `program_kegiatan_fungsional` */

/*Table structure for table `program_kegiatan_struktural` */

DROP TABLE IF EXISTS `program_kegiatan_struktural`;

CREATE TABLE `program_kegiatan_struktural` (
  `id_program_kegiatan_struktural` bigint(30) NOT NULL AUTO_INCREMENT,
  `indikator_sasaran_id` bigint(30) DEFAULT NULL,
  `uraian_program_kegiatan` text,
  `jumlah_anggaran` double(15,0) DEFAULT NULL,
  `sumber_anggaran_id` int(11) DEFAULT NULL,
  `type_input` enum('0','1') DEFAULT NULL COMMENT '1 = perubahan, 0 = tidak',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_program_kegiatan_struktural`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `program_kegiatan_struktural` */

insert  into `program_kegiatan_struktural`(`id_program_kegiatan_struktural`,`indikator_sasaran_id`,`uraian_program_kegiatan`,`jumlah_anggaran`,`sumber_anggaran_id`,`type_input`,`created_at`,`updated_at`,`deleted_at`) values 
(2,3,'program kedua',1500000,2,'0','2020-09-29 09:56:42','2020-09-29 10:11:30','2020-09-29 10:17:29'),
(3,3,'prog keg 1',2000000,1,'0','2020-09-29 03:05:01',NULL,NULL);

/*Table structure for table `sasaran` */

DROP TABLE IF EXISTS `sasaran`;

CREATE TABLE `sasaran` (
  `id_sasaran` bigint(30) NOT NULL AUTO_INCREMENT,
  `unor` varchar(10) DEFAULT NULL,
  `pns_nip` varchar(20) DEFAULT NULL,
  `tahun` char(4) DEFAULT NULL,
  `uraian_sasaran` text,
  `type_input` enum('0','1') DEFAULT NULL COMMENT '1 = perubahan, 0 = tidak',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_sasaran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sasaran` */

insert  into `sasaran`(`id_sasaran`,`unor`,`pns_nip`,`tahun`,`uraian_sasaran`,`type_input`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'8804000000','196903101994031010','2020','sasaran pertama','0','2020-09-21 07:52:29',NULL,NULL),
(2,'8804000000','196903101994031010','2020','sasaran baru ubah','0','2020-09-21 07:56:13','2020-09-24 08:04:26','2020-09-24 08:07:46'),
(3,'8804000000','196903101994031010','2020','sasaran kedua','0','2020-09-24 08:04:37','2020-09-24 08:04:45',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level_user_id` int(11) DEFAULT NULL,
  `foto_user` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `fk_level_user` (`level_user_id`),
  CONSTRAINT `fk_level_user` FOREIGN KEY (`level_user_id`) REFERENCES `level_user` (`id_level_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nama_lengkap`,`username`,`password`,`level_user_id`,`foto_user`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Superadmin','superadmin','$2y$12$2d2GEFgIkv/WhniVgh4aFuQFRLryQ1BOn6CnZBmft2XP2WR.MP7ua',1,'d428e7d67751869b72a9db2adc8cb0d3.jpg',NULL,'2020-09-08 05:17:46',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
