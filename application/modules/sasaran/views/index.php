<style>
    .table-filter tr{
        display:block !important;
    } 

    .table-filter tr td:first-of-type{
        width:60px;
    }

    .table-filter tr td:nth-child(2){
        width:30px;
    } 

    .filter-select{
        width:330px;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <table class="table-filter">
                <tr>
                    <td>Tahun</td>
                    <td>:</td>
                    <td>
                        <select class="form-control select-search" name="tahun" onChange="get_data_sasaran()">
                            <?php
                            echo ($show_option?"<option value=''>-- PILIH TAHUN -- </option>":"");
                            foreach($list_tahun as $key=>$row){
                                $selected = "";
                                if($row == date('Y')){
                                    $selected = "selected";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row; ?>"><?php echo $row; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>SKPD</td>
                    <td>:</td>
                    <td class="filter-select">
                        <select class="form-control select-search" name="skpd" onChange="get_data_pns()">
                            <option value="">-- PILIH SKPD --</option>
                            <?php
                            foreach($list_skpd as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->KD_UNOR); ?>"><?php echo $row->NM_UNOR; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>PNS</td>
                    <td>:</td>
                    <td class="filter-select">
                        <select class="form-control select-search" name="pns" onChange="get_data_sasaran()">
                            <option value="">-- PILIH PNS --</option>
                        </select>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="box-top text-right">
                <a href="#tambahSasaran" class="btn btn-info">Tambah Sasaran</a>
            </div>
        </div>
        <table id="datatableSasaran" class="table datatable-save-state table-bordered table-striped table-xs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Sasaran</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modal_form_sasaran" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title"><span class="header_name_modal"></span> Sasaran</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <fieldset class="mb-3">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Tahun <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" readonly class="form-control" name="tahun">
                            <input type="hidden" name="id_sasaran" />
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">SKPD <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" readonly class="form-control" name="skpd">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">PNS <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" readonly class="form-control" name="pns">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Sasaran <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->uraian_sasaran:""; ?>" name="sasaran" required placeholder="Sasaran">
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn bg-primary" onClick="btn_act_sasaran()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
$(".box-top").hide();

let datatableSasaran = $("#datatableSasaran").DataTable();
function get_data_sasaran(){
    $(".box-top").hide();

    let tahun = $("select[name=tahun]").val();
    let skpd = $("select[name=skpd]").val();
    let pns = $("select[name=pns]").val();

    datatableSasaran.clear().draw();
    if(tahun && skpd && pns){
        $(".box-top").show();

        $.ajax({
            url: base_url+'sasaran/request/get_data_sasaran',
            data:{tahun:tahun,skpd:skpd,pns:pns},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let no = 1;
                $.each(response,function(index,value){
                    datatableSasaran.row.add([
                        no,
                        value.uraian_sasaran,
                        "<a href='#' onclick=\"open_form('"+value.id_encrypt+"')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function confirm_delete(id_sasaran){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'sasaran/delete_sasaran',
                data : {id_sasaran:id_sasaran},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_sasaran();
                        swalInit(
                            'Berhasil',
                            'Data berhasil dihapus',
                            'success'
                        );
                    }else{
                        get_data_sasaran();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_sasaran();
                }
            });
        }
    });
}

function get_data_pns(){
    let skpd = $("select[name=skpd]").val();
    datatableSasaran.clear().draw();
    $("select[name=pns]").html("<option value=''>-- PILIH PNS --</option>");
    if(skpd){
        $.ajax({
            url: base_url+'sasaran/request/get_data_pns',
            data:{skpd:skpd},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let html = "";
                html += "<option value=''>-- PILIH PNS --</option>";
                $.each(response,function(index,value){
                    html += "<option value='"+value.pns_nip_encrypt+"'>"+value.PNS_PNSNAM+"</option>";
                });
                $("select[name=pns]").html(html);
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

$("a[href$='#tambahSasaran']").on("click",function(){
    let tahun = $("select[name=tahun] option:selected").html();
    let skpd = $("select[name=skpd] option:selected").html();
    let pns = $("select[name=pns] option:selected").html();

    $("#modal_form_sasaran").modal("show");

    $("input[name=tahun]").val(tahun);
    $("input[name=skpd]").val(skpd);
    $("input[name=pns]").val(pns);

    $(".header_name_modal").html("Tambah");
});

function btn_act_sasaran(){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    let tahun = $("select[name=tahun]").val();
    let skpd = $("select[name=skpd]").val();
    let pns = $("select[name=pns]").val();
    let sasaran = $("input[name=sasaran]").val();
    let id_sasaran = $("input[name=id_sasaran]").val();

    if(tahun && skpd && pns){
        if(sasaran){
            $.ajax({
                url: base_url+'sasaran/act_sasaran_strutkural',
                data:{tahun:tahun,skpd:skpd,pns:pns,sasaran:sasaran,id_sasaran:id_sasaran},
                type: 'POST',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    $("#modal_form_sasaran").modal("toggle");

                    $("input[name=tahun]").val("");
                    $("input[name=skpd]").val("");
                    $("input[name=pns]").val("");
                    $("input[name=sasaran]").val("");
                    $("input[name=id_sasaran]").val("");
                    get_data_sasaran();

                    if(id_sasaran){
                        swalInit(
                            'Berhasil',
                            'Data berhasil diubah!',
                            'success'
                        );
                    }else{
                        swalInit(
                            'Berhasil',
                            'Data baru berhasil ditambahkan!',
                            'success'
                        );
                    }
                },
                complete:function(){
                    loading_stop();
                }
            });
        }else{
            swalInit(
                'Gagal',
                'Sasaran tidak boleh kosong!',
                'error'
            );
        }
    }
}

function open_form(id_sasaran){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    $.ajax({
        url: base_url+'sasaran/request/get_data_sasaran_by_id',
        data:{id_sasaran:id_sasaran},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $("#modal_form_sasaran").modal("show");

            $("input[name=tahun]").val(response.tahun);
            $("input[name=skpd]").val(response.skpd);
            $("input[name=pns]").val(response.nama_pns);
            $("input[name=sasaran]").val(response.uraian_sasaran);
            $("input[name=id_sasaran]").val(id_sasaran);

            $(".header_name_modal").html("Ubah");
        },
        complete:function(){
            loading_stop();
        }
    });
}

</script>