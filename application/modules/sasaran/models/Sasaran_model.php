<?php

class Sasaran_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="sasaran";
        $this->primary_id="id_sasaran";

        $this->simk = $this->db->database;

        $this->presensi = $this->config->item('presensi');

    }

    public function get_data_sasaran($id_sasaran){
        $this->db->select("{$this->simk}.sasaran.*,{$this->presensi}.unor.NM_UNOR AS skpd,{$this->presensi}.pns.PNS_PNSNAM AS nama_pns");
        $this->db->join("{$this->presensi}.pns", "{$this->presensi}.pns.PNS_PNSNIP={$this->simk}.sasaran.pns_nip");
        $this->db->join("{$this->presensi}.unor", "{$this->presensi}.unor.KD_UNOR={$this->simk}.sasaran.unor");
        $this->db->where("{$this->simk}.sasaran.id_sasaran",$id_sasaran);
        $this->db->where("{$this->simk}.sasaran.deleted_at",NULL);
        return $this->db->get("{$this->simk}.sasaran")->row();
    }
}