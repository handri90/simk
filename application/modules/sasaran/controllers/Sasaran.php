<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sasaran extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('presensi_model');
		$this->load->model('sasaran_model');
		$this->load->model('master_periode/master_periode_model','master_periode_model');
	}

	public function struktural()
	{
        $list_tahun = $this->master_periode_model->get(
            array(
                "where"=>array(
                    "periode_is_active"=>"1"
                )
            ),"row"
        );

        $year_now = date("Y");

        $tahun_arr = array();

        for($i = $list_tahun->tahun_awal;$i<=$list_tahun->tahun_akhir;$i++){
            if($i<=$year_now){
                array_push($tahun_arr,$i);
            }
        }

        $data['show_option'] = false;
        if(!$tahun_arr){
            $data['show_option'] = true;
        }

        $data['list_tahun'] = $tahun_arr;

        $data['list_skpd'] = $this->presensi_model->get_data_skpd();

        $data['breadcrumb'] = [['link'=>false,'content'=>'Sasaran','is_active'=>true]];
        $this->execute('index',$data);
    }
    
    public function perubahan()
	{
        $list_tahun = $this->master_periode_model->get(
            array(
                "where"=>array(
                    "periode_is_active"=>"1"
                )
            ),"row"
        );

        $year_now = date("Y");

        $tahun_arr = array();

        for($i = $list_tahun->tahun_awal;$i<=$list_tahun->tahun_akhir;$i++){
            if($i<=$year_now){
                array_push($tahun_arr,$i);
            }
        }

        $data['show_option'] = false;
        if(!$tahun_arr){
            $data['show_option'] = true;
        }

        $data['list_tahun'] = $tahun_arr;

        $data['list_skpd'] = $this->sasaran_model->get_data_skpd();

        $data['breadcrumb'] = [['link'=>false,'content'=>'Sasaran Perubahan','is_active'=>true]];
        $this->execute('index',$data);
    }

    public function act_sasaran_strutkural(){
        $tahun = $this->ipost("tahun");
        $skpd = decrypt_data($this->ipost("skpd"));
        $pns = decrypt_data($this->ipost("pns"));
        $sasaran = $this->ipost("sasaran");
        $id_sasaran = decrypt_data($this->ipost("id_sasaran"));
        
        if($id_sasaran){
            $data = array(
                'uraian_sasaran'=>$sasaran,
                'updated_at'=>$this->datetime()
            );
    
            $status = $this->sasaran_model->edit($id_sasaran,$data);
        }else{
            $data = array(
                "unor"=>$skpd,
                "pns_nip"=>$pns,
                'tahun'=>$tahun,
                'uraian_sasaran'=>$sasaran,
                "type_input"=>"0",
                'created_at'=>$this->datetime()
            );
    
            $status = $this->sasaran_model->save($data);
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function delete_sasaran(){
        $id_sasaran = $this->iget('id_sasaran');
        $data_master = $this->sasaran_model->get_by(decrypt_data($id_sasaran));

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->sasaran_model->remove(decrypt_data($id_sasaran));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
