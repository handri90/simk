<?php

class Indikator_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="indikator_sasaran";
        $this->primary_id="id_indikator_sasaran";
        
        $this->simk = $this->db->database;

        $this->kinerja = $this->config->item('kinerja');
        $this->presensi = $this->config->item('presensi');

    }

    public function get_data_indikator($id_sasaran){
        $this->db->select("{$this->simk}.indikator_sasaran.*,{$this->kinerja}.satuan.nama AS nama_satuan");
        $this->db->join("{$this->kinerja}.satuan", "{$this->kinerja}.satuan.id={$this->simk}.indikator_sasaran.satuan_id");
        $this->db->where("{$this->simk}.indikator_sasaran.sasaran_id",$id_sasaran);
        $this->db->where("{$this->simk}.indikator_sasaran.deleted_at",NULL);
        $this->db->where("{$this->simk}.indikator_sasaran.type_input","0");
        return $this->db->get("{$this->simk}.indikator_sasaran")->result();
    }
    
    public function get_data_indikator_by_id($id_indikator){
        $this->db->select("{$this->simk}.indikator_sasaran.*,{$this->presensi}.unor.NM_UNOR AS skpd,{$this->presensi}.pns.PNS_PNSNAM AS nama_pns,{$this->simk}.sasaran.uraian_sasaran,{$this->simk}.sasaran.tahun");
        $this->db->join("{$this->simk}.sasaran", "{$this->simk}.sasaran.id_sasaran={$this->simk}.indikator_sasaran.sasaran_id");
        $this->db->join("{$this->presensi}.pns", "{$this->presensi}.pns.PNS_PNSNIP={$this->simk}.sasaran.pns_nip");
        $this->db->join("{$this->presensi}.unor", "{$this->presensi}.unor.KD_UNOR={$this->simk}.sasaran.unor");
        $this->db->where("{$this->simk}.indikator_sasaran.id_indikator_sasaran",$id_indikator);
        $this->db->where("{$this->simk}.indikator_sasaran.deleted_at",NULL);
        $this->db->where("{$this->simk}.sasaran.deleted_at",NULL);
        return $this->db->get("{$this->simk}.indikator_sasaran")->row();
    }

    
}