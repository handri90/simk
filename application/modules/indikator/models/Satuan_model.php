<?php

class Satuan_model extends MY_Model{

    function __construct(){
        parent::__construct();

        $this->dbkinerja = $this->load->database($this->config->item('kinerja'),true);
        
        $this->kinerja = $this->config->item('kinerja');

    }

    public function get_data_satuan(){
        $this->dbkinerja->select("{$this->kinerja}.satuan.*");
        return $this->dbkinerja->get("{$this->kinerja}.satuan")->result();
    }
}