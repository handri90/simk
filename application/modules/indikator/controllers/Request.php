<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->model('presensi_model');
        $this->load->model('indikator_model');
		$this->load->model('satuan_model');
		$this->load->model('sasaran/sasaran_model','sasaran_model');
    }
    
    public function get_data_indikator(){
        $sasaran = decrypt_data($this->iget("sasaran"));

        $data_indikator = $this->indikator_model->get_data_indikator($sasaran);

        $templist = array();
        foreach($data_indikator as $key=>$row){
            $jumlah = 0;
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_indikator_sasaran);
            $templist[$key]['jumlah'] = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6 + $row->m_7 + $row->m_8 + $row->m_9 + $row->m_10 + $row->m_11 + $row->m_12;
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

	public function get_data_sasaran(){
        $tahun = $this->iget("tahun");
        $pns = decrypt_data($this->iget("pns"));
        $skpd = decrypt_data($this->iget("skpd"));

        $data_sasaran = $this->sasaran_model->get(
            array(
                "where"=>array(
                    "unor"=>$skpd,
                    "tahun"=>$tahun,
                    "pns_nip"=>$pns
                )
            )
        );

        $templist = array();
        foreach($data_sasaran as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_sasaran);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_pns(){
        $skpd = decrypt_data($this->iget("skpd"));

        $data_pns = $this->presensi_model->get_data_pns($skpd);

        $templist = array();
        foreach($data_pns as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['pns_nip_encrypt'] = encrypt_data($row->PNS_PNSNIP);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_indikator_by_id(){
        $id_indikator = decrypt_data($this->iget("id_indikator"));

        $data = $this->indikator_model->get_data_indikator_by_id($id_indikator);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_satuan(){
        $data_satuan = $this->satuan_model->get_data_satuan();

        $templist = array();
        foreach($data_satuan as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id);
        }

        $data = $templist;

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
