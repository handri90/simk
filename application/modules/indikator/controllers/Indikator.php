<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikator extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('presensi_model');
		$this->load->model('indikator_model');
        $this->load->model('master_periode/master_periode_model','master_periode_model');
	}

	public function index()
	{
        $list_tahun = $this->master_periode_model->get(
            array(
                "where"=>array(
                    "periode_is_active"=>"1"
                )
            ),"row"
        );

        $year_now = date("Y");

        $tahun_arr = array();

        for($i = $list_tahun->tahun_awal;$i<=$list_tahun->tahun_akhir;$i++){
            if($i<=$year_now){
                array_push($tahun_arr,$i);
            }
        }

        $data['show_option'] = false;
        if(!$tahun_arr){
            $data['show_option'] = true;
        }

        $data['list_tahun'] = $tahun_arr;

        $data['list_skpd'] = $this->presensi_model->get_data_skpd();
        
        $data['breadcrumb'] = [['link'=>false,'content'=>'Indikator','is_active'=>true]];
        $this->execute('index',$data);
    }

    public function act_indikator_struktural(){
        $sasaran = decrypt_data($this->ipost("sasaran"));
        $indikator = $this->ipost("indikator");
        $id_indikator = decrypt_data($this->ipost("id_indikator"));
        $satuan = decrypt_data($this->ipost("satuan"));
        $m_1 = $this->ipost("m_1");
        $m_2 = $this->ipost("m_2");
        $m_3 = $this->ipost("m_3");
        $m_4 = $this->ipost("m_4");
        $m_5 = $this->ipost("m_5");
        $m_6 = $this->ipost("m_6");
        $m_7 = $this->ipost("m_7");
        $m_8 = $this->ipost("m_8");
        $m_9 = $this->ipost("m_9");
        $m_10 = $this->ipost("m_10");
        $m_11 = $this->ipost("m_11");
        $m_12 = $this->ipost("m_12");
        
        if($id_indikator){
            $data = array(
                'sasaran_id'=>$sasaran,
                'uraian_indikator'=>$indikator,
                'satuan_id'=>$satuan,
                'm_1'=>$m_1,
                'm_2'=>$m_2,
                'm_3'=>$m_3,
                'm_4'=>$m_4,
                'm_5'=>$m_5,
                'm_6'=>$m_6,
                'm_7'=>$m_7,
                'm_8'=>$m_8,
                'm_9'=>$m_9,
                'm_10'=>$m_10,
                'm_11'=>$m_11,
                'm_12'=>$m_12,
                'updated_at'=>$this->datetime()
            );
    
            $status = $this->indikator_model->edit($id_indikator,$data);
        }else{
            $data = array(
                'sasaran_id'=>$sasaran,
                'uraian_indikator'=>$indikator,
                'satuan_id'=>$satuan,
                'm_1'=>$m_1,
                'm_2'=>$m_2,
                'm_3'=>$m_3,
                'm_4'=>$m_4,
                'm_5'=>$m_5,
                'm_6'=>$m_6,
                'm_7'=>$m_7,
                'm_8'=>$m_8,
                'm_9'=>$m_9,
                'm_10'=>$m_10,
                'm_11'=>$m_11,
                'm_12'=>$m_12,
                'type_input'=>"0",
                'created_at'=>$this->datetime()
            );
    
            $status = $this->indikator_model->save($data);
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function delete_indikator(){
        $id_indikator = $this->iget('id_indikator');
        $data_master = $this->indikator_model->get_by(decrypt_data($id_indikator));

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->indikator_model->remove(decrypt_data($id_indikator));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
