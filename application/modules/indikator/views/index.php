<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tahun : </label>
                        <select class="form-control select-search" name="tahun">
                            <?php
                            echo ($show_option?"<option value=''>-- PILIH TAHUN --</option>":"");
                            foreach($list_tahun as $key=>$row){
                                $selected = "";
                                if($row == date('Y')){
                                    $selected = "selected";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row; ?>"><?php echo $row; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>SKPD : </label>
                        <select class="form-control select-search" name="skpd" onChange="get_data_pns()">
                            <option value="">-- PILIH SKPD --</option>
                            <?php
                            foreach($list_skpd as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->KD_UNOR); ?>"><?php echo $row->NM_UNOR; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>PNS : </label>
                        <select class="form-control select-search" name="pns" onChange="get_data_sasaran()">
                            <option value="">-- PILIH PNS --</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Sasaran : </label>
                        <select class="form-control select-search" name="sasaran" onChange="get_data_indikator()">
                            <option value="">-- PILIH Sasaran --</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="box-top text-right">
                <a href="#tambahIndikator" class="btn btn-info">Tambah Indikator</a>
            </div>
        </div>
        <table id="datatableIndikator" class="table datatable-save-state table-bordered table-striped table-xs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Indikator</th>
                    <th>Satuan</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>Jumlah</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modal_form_indikator" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title"><span class="header_name_modal"></span> Indikator</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="mb-3">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Tahun <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" readonly class="form-control" name="tahun">
                                    <input type="hidden" name="id_indikator" />
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">SKPD <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" readonly class="form-control" name="skpd">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">PNS <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" readonly class="form-control" name="pns">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Sasaran <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" readonly class="form-control" name="sasaran" placeholder="Sasaran">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Indikator <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="indikator" placeholder="Indikator">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Satuan <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                <select class="form-control select-search" name="satuan">
                                    <option value="">-- Pilih Satuan --</option>
                                    <?php
                                    foreach($list_satuan as $key=>$row){
                                        ?>
                                        <option value="<?php echo encrypt_data($row->id); ?>"><?php echo $row->nama; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="mb-3">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 1 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_1" placeholder="Bulan 1">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 2 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_2" placeholder="Bulan 2">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 3 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_3" placeholder="Bulan 3">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 4 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_4" placeholder="Bulan 4">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 5 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_5" placeholder="Bulan 5">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 6 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_6" placeholder="Bulan 6">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="mb-3">                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 7 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_7" placeholder="Bulan 7">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 8 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_8" placeholder="Bulan 8">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 9 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_9" placeholder="Bulan 9">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 10 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_10" placeholder="Bulan 10">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 11 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_11" placeholder="Bulan 11">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Bulan 12 <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="m_12" placeholder="Bulan 12">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn bg-primary" onClick="btn_act_indikator()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
$(".box-top").hide();

let datatableIndikator = $("#datatableIndikator").DataTable();

function get_data_indikator(){
    $(".box-top").hide();

    let sasaran = $("select[name=sasaran]").val();

    datatableIndikator.clear().draw();
    if(sasaran){
        $(".box-top").show();

        $.ajax({
            url: base_url+'indikator/request/get_data_indikator',
            data:{sasaran:sasaran},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let no = 1;
                $.each(response,function(index,value){
                    datatableIndikator.row.add([
                        no,
                        value.uraian_indikator,
                        value.nama_satuan,
                        value.m_1,
                        value.m_2,
                        value.m_3,
                        value.m_4,
                        value.m_5,
                        value.m_6,
                        value.m_7,
                        value.m_8,
                        value.m_9,
                        value.m_10,
                        value.m_11,
                        value.m_12,
                        value.jumlah,
                        "<a href='#' onclick=\"open_form('"+value.id_encrypt+"')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function get_data_sasaran(){

    let tahun = $("select[name=tahun]").val();
    let skpd = $("select[name=skpd]").val();
    let pns = $("select[name=pns]").val();

    datatableIndikator.clear().draw();
    if(tahun && skpd && pns){
        $("select[name=sasaran]").html("<option value=''>-- Pilih Sasaran --</option>");
        $.ajax({
            url: base_url+'indikator/request/get_data_sasaran',
            data:{tahun:tahun,skpd:skpd,pns:pns},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let html = "";
                html += "<option value=''>-- Pilih Sasaran --</option>";
                $.each(response,function(index,value){
                    html += "<option value='"+value.id_encrypt+"'>"+value.uraian_sasaran+"</option>";
                });
                $("select[name=sasaran]").html(html);
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function confirm_delete(id_indikator){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'indikator/delete_indikator',
                data : {id_indikator:id_indikator},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    get_data_indikator();
                    if(response){
                        swalInit(
                            'Berhasil',
                            'Data berhasil dihapus',
                            'success'
                        );
                    }else{
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_indikator();
                }
            });
        }
    });
}

function get_data_pns(){
    let skpd = $("select[name=skpd]").val();
    datatableIndikator.clear().draw();
    $("select[name=pns]").html("<option value=''>-- PILIH PNS --</option>");
    if(skpd){
        $.ajax({
            url: base_url+'indikator/request/get_data_pns',
            data:{skpd:skpd},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let html = "";
                html += "<option value=''>-- PILIH PNS --</option>";
                $.each(response,function(index,value){
                    html += "<option value='"+value.pns_nip_encrypt+"'>"+value.PNS_PNSNAM+"</option>";
                });
                $("select[name=pns]").html(html);
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

$("a[href$='#tambahIndikator']").on("click",function(){
    let tahun = $("select[name=tahun] option:selected").html();
    let skpd = $("select[name=skpd] option:selected").html();
    let pns = $("select[name=pns] option:selected").html();
    let sasaran = $("select[name=sasaran] option:selected").html();

    $("#modal_form_indikator").modal("show");

    $("input[name=tahun]").val(tahun);
    $("input[name=skpd]").val(skpd);
    $("input[name=pns]").val(pns);
    $("input[name=sasaran]").val(sasaran);

    $(".header_name_modal").html("Tambah");

    get_data_satuan();
});

function btn_act_indikator(){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    let sasaran = $("select[name=sasaran]").val();
    let indikator = $("input[name=indikator]").val();
    let id_indikator = $("input[name=id_indikator]").val();
    let satuan = $("select[name=satuan]").val();
    let m_1 = $("input[name=m_1]").val();
    let m_2 = $("input[name=m_2]").val();
    let m_3 = $("input[name=m_3]").val();
    let m_4 = $("input[name=m_4]").val();
    let m_5 = $("input[name=m_5]").val();
    let m_6 = $("input[name=m_6]").val();
    let m_7 = $("input[name=m_7]").val();
    let m_8 = $("input[name=m_8]").val();
    let m_9 = $("input[name=m_9]").val();
    let m_10 = $("input[name=m_10]").val();
    let m_11 = $("input[name=m_11]").val();
    let m_12 = $("input[name=m_12]").val();

    if(indikator){
        if(sasaran){
            $.ajax({
                url: base_url+'indikator/act_indikator_struktural',
                data:{sasaran:sasaran,id_indikator:id_indikator,indikator:indikator,satuan:satuan,m_1:m_1,m_2:m_2,m_3:m_3,m_4:m_4,m_5:m_5,m_6:m_6,m_7:m_7,m_8:m_8,m_9:m_9,m_10:m_10,m_11:m_11,m_12:m_12},
                type: 'POST',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    $("#modal_form_indikator").modal("toggle");

                    $("input[name=tahun]").val("");
                    $("input[name=skpd]").val("");
                    $("input[name=pns]").val("");
                    $("input[name=sasaran]").val("");
                    $("input[name=indikator]").val("");
                    $("select[name=satuan]").html("<option value=''>-- Pilih Satuan --</option>");
                    $("input[name=id_indikator]").val("");
                    $("input[name=m_1]").val("");
                    $("input[name=m_2]").val("");
                    $("input[name=m_3]").val("");
                    $("input[name=m_4]").val("");
                    $("input[name=m_5]").val("");
                    $("input[name=m_6]").val("");
                    $("input[name=m_7]").val("");
                    $("input[name=m_8]").val("");
                    $("input[name=m_9]").val("");
                    $("input[name=m_10]").val("");
                    $("input[name=m_11]").val("");
                    $("input[name=m_12]").val("");
                    get_data_indikator();

                    if(id_indikator){
                        swalInit(
                            'Berhasil',
                            'Data berhasil diubah!',
                            'success'
                        );
                    }else{
                        swalInit(
                            'Berhasil',
                            'Data baru berhasil ditambahkan!',
                            'success'
                        );
                    }
                },
                complete:function(){
                    loading_stop();
                }
            });
        }else{
            swalInit(
                'Gagal',
                'Sasaran tidak boleh kosong!',
                'error'
            );
        }
    }else{
        swalInit(
            'Gagal',
            'Field tidak boleh kosong!',
            'error'
        );
    }
}

function open_form(id_indikator){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    $.ajax({
        url: base_url+'indikator/request/get_data_indikator_by_id',
        data:{id_indikator:id_indikator},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $("#modal_form_indikator").modal("show");

            $("input[name=tahun]").val(response.tahun);
            $("input[name=skpd]").val(response.skpd);
            $("input[name=pns]").val(response.nama_pns);
            $("input[name=sasaran]").val(response.uraian_sasaran);
            $("input[name=indikator]").val(response.uraian_indikator);
            $("input[name=id_indikator]").val(id_indikator);
            $("input[name=m_1]").val(response.m_1);
            $("input[name=m_2]").val(response.m_2);
            $("input[name=m_3]").val(response.m_3);
            $("input[name=m_4]").val(response.m_4);
            $("input[name=m_5]").val(response.m_5);
            $("input[name=m_6]").val(response.m_6);
            $("input[name=m_7]").val(response.m_7);
            $("input[name=m_8]").val(response.m_8);
            $("input[name=m_9]").val(response.m_9);
            $("input[name=m_10]").val(response.m_10);
            $("input[name=m_11]").val(response.m_11);
            $("input[name=m_12]").val(response.m_12);

            $(".header_name_modal").html("Ubah");
            get_data_satuan(response.satuan_id);
        },
        complete:function(){
            loading_stop();
        }
    });
}

function get_data_satuan(id_satuan_selected){
    $("select[name=satuan]").html("<option value=''>-- Pilih Satuan --</option>");
    $.ajax({
        url: base_url+'indikator/request/get_data_satuan',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            let html = "";
            html += "<option value=''>-- Pilih Satuan --</option>";
            $.each(response,function(index,value){
                let selected = "";
                if(id_satuan_selected == value.id){
                    selected = "selected";
                }
                html += "<option "+selected+" value='"+value.id_encrypt+"'>"+value.nama+"</option>";
            });
            $("select[name=satuan]").html(html);
        },
        complete:function(){
            loading_stop();
        }
    });
}

</script>