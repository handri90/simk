<?php

class Penilaian_anggaran_struktural_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="penilaian_anggaran_struktural";
        $this->primary_id="id_penilaian_anggaran_struktural";

        $this->dbkinerja = $this->load->database($this->config->item('kinerja'),true);
        
        $this->simk = $this->db->database;

        $this->kinerja = $this->config->item('kinerja');
        $this->presensi = $this->config->item('presensi');

    }

    public function get_data_penilaian($skpd,$pns,$tahun){
        $this->db->select("{$this->simk}.program_kegiatan_struktural.*,{$this->simk}.indikator_sasaran.*,{$this->simk}.sasaran.*,{$this->kinerja}.satuan.*");
        $this->db->join("{$this->simk}.indikator_sasaran", "{$this->simk}.indikator_sasaran.id_indikator_sasaran={$this->simk}.program_kegiatan_struktural.indikator_sasaran_id");
        $this->db->join("{$this->simk}.sasaran", "{$this->simk}.sasaran.id_sasaran={$this->simk}.indikator_sasaran.sasaran_id");
        $this->db->join("{$this->presensi}.pns", "{$this->presensi}.pns.PNS_PNSNIP={$this->simk}.sasaran.pns_nip");
        $this->db->join("{$this->presensi}.unor", "{$this->presensi}.unor.KD_UNOR={$this->simk}.sasaran.unor");
        $this->db->join("{$this->kinerja}.satuan", "{$this->kinerja}.satuan.id={$this->simk}.indikator_sasaran.satuan_id");
        $this->db->where("{$this->simk}.sasaran.tahun",$tahun);
        $this->db->where("{$this->simk}.sasaran.unor",$skpd);
        $this->db->where("{$this->simk}.sasaran.pns_nip",$pns);
        $this->db->where("{$this->simk}.program_kegiatan_struktural.deleted_at",NULL);
        $this->db->where("{$this->simk}.indikator_sasaran.deleted_at",NULL);
        $this->db->where("{$this->simk}.sasaran.deleted_at",NULL);
        return $this->db->get("{$this->simk}.program_kegiatan_struktural")->result();
    }
}