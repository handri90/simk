<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tahun : </label>
                        <select class="form-control select-search" name="tahun" onchange="get_data_penilaian()">
                            <?php
                            echo ($show_option?"<option value=''>-- Pilih Tahun --</option>":"");
                            foreach($list_tahun as $key=>$row){
                                $selected = "";
                                if($row == date('Y')){
                                    $selected = "selected";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row; ?>"><?php echo $row; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>SKPD : </label>
                        <select class="form-control select-search" name="skpd" onChange="get_data_pns()">
                            <option value="">-- Pilih SKPD --</option>
                            <?php
                            foreach($list_skpd as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->KD_UNOR); ?>"><?php echo $row->NM_UNOR; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>PNS : </label>
                        <select class="form-control select-search" name="pns" onChange="get_data_penilaian()">
                            <option value="">-- Pilih PNS --</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>S/D Bulan : </label>
                        <select class="form-control select-search" name="bulan" onChange="get_data_penilaian()">
                            <?php
                            foreach($list_bulan as $key=>$row){
                                $selected = "";
                                if(date("n") == $row){
                                    $selected = "selected";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row; ?>"><?php echo $row; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <table id="datatablePenilaian" class="table datatable-save-state table-bordered table-striped table-xs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Sasaran</th>
                    <th>Indikator Kinerja</th>
                    <th>Satuan</th>
                    <th>Target</th>
                    <th>Realisasi</th>
                    <th>%</th>
                    <th>Kinerja Proses</th>
                    <th>Program/Kegiatan</th>
                    <th>Pagu</th>
                    <th>Realisasi</th>
                    <th>%</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modal_form_penilaian" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title">Tambah Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <fieldset class="mb-3">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Tahun <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="tahun">
                            <input type="hidden" name="id_indikator" />
                            <input type="hidden" name="id_program_kegiatan" />
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">SKPD <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="skpd">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">PNS <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="pns">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Sasaran <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="sasaran">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Indikator <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="indikator">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Target/Realisasi Output <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="text" readonly class="form-control" name="target">
                                </div>
                                /
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="realisasi_output">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Program/Kegiatan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" readonly name="program_kegiatan" required placeholder="Program Kegiatan">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Pagu/Realisasi Anggaran (Rp.) <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" readonly class="form-control" name="pagu" required placeholder="Pagu">
                                </div>
                                /
                                <div class="col-md-5">
                                    <input type="text" class="form-control input-currency" name="pagu_realisasi" required placeholder="Realisasi Anggaran">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Sumber Anggaran <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" disabled name="sumber_anggaran">
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn bg-primary" onClick="btn_act_penilaian()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
$(".box-top").hide();

const IDR = value => currency(
    value, { 
        symbol: "",
        precision: 0,
        separator: "." 
});

var cleave = new Cleave('.input-currency', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.',
});

let datatablePenilaian = $("#datatablePenilaian").DataTable();

function get_data_penilaian(){
    $(".box-top").hide();

    let tahun = $("select[name=tahun]").val();
    let bulan = $("select[name=bulan]").val();
    let skpd = $("select[name=skpd]").val();
    let pns = $("select[name=pns]").val();

    datatablePenilaian.clear().draw();
    if(tahun && skpd && pns && bulan){
        $(".box-top").show();

        $.ajax({
            url: base_url+'penilaian/request/get_data_penilaian',
            data:{tahun:tahun,skpd:skpd,pns:pns,bulan:bulan},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let no = 1;
                $.each(response,function(index,value){
                    datatablePenilaian.row.add([
                        no,
                        value.uraian_sasaran,
                        value.uraian_indikator,
                        value.nama,
                        value.target,
                        value.realisasi,
                        value.persentase_target_realisasi,
                        "",
                        value.uraian_program_kegiatan,
                        IDR(value.jumlah_anggaran).format(true),
                        IDR(value.realisasi_anggaran).format(true),
                        value.persentase_realisasi_anggaran,
                        "<a href='#' onclick=\"open_form('"+value.id_indikator_encrypt+"','"+value.id_program_kegiatan_encrypt+"','"+tahun+"','"+skpd+"','"+pns+"','"+bulan+"')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function get_data_pns(){
    let skpd = $("select[name=skpd]").val();
    datatablePenilaian.clear().draw();
    $("select[name=pns]").html("<option value=''>-- Pilih PNS --</option>");
    if(skpd){
        $.ajax({
            url: base_url+'penilaian/request/get_data_pns',
            data:{skpd:skpd},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let html = "";
                html += "<option value=''>-- Pilih PNS --</option>";
                $.each(response,function(index,value){
                    html += "<option value='"+value.pns_nip_encrypt+"'>"+value.PNS_PNSNAM+"</option>";
                });
                $("select[name=pns]").html(html);
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function btn_act_penilaian(){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    let id_indikator = $("input[name=id_indikator]").val();
    let id_program_kegiatan = $("input[name=id_program_kegiatan]").val();
    let bulan = $("select[name=bulan]").val();
    let realisasi_output = $("input[name=realisasi_output]").val();
    let pagu_realisasi = $("input[name=pagu_realisasi]").val();

    if(id_indikator && id_program_kegiatan && bulan && realisasi_output && pagu_realisasi){
        $.ajax({
            url: base_url+'penilaian/act_penilaian',
            data:{id_indikator:id_indikator,id_program_kegiatan:id_program_kegiatan,bulan:bulan,realisasi_output:realisasi_output,pagu_realisasi:pagu_realisasi},
            type: 'POST',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                $("#modal_form_penilaian").modal("toggle");

                $("input[name=tahun]").val("");
                $("input[name=skpd]").val("");
                $("input[name=pns]").val("");
                $("input[name=sasaran]").val("");
                $("input[name=indikator]").val("");
                $("input[name=target]").val("");
                $("input[name=realisasi_output]").val("");
                $("input[name=program_kegiatan]").val("");
                $("input[name=pagu]").val("");
                $("input[name=pagu_realisasi]").val("");
                $("input[name=id_indikator]").val("");
                $("input[name=id_program_kegiatan]").val("");

                get_data_penilaian();

                swalInit(
                    'Berhasil',
                    'Data berhasil diubah!',
                    'success'
                );
            },
            complete:function(){
                loading_stop();
            }
        });
    }else{
        swalInit(
            'Gagal',
            'Field tidak boleh kosong!',
            'error'
        );
    }
}

function open_form(id_indikator,id_program_kegiatan,tahun,skpd,pns,bulan){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    $.ajax({
        url: base_url+'penilaian/request/get_data_penilaian_by_id',
        data:{id_indikator:id_indikator,id_program_kegiatan:id_program_kegiatan,tahun:tahun,skpd:skpd,pns:pns,bulan:bulan},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $("#modal_form_penilaian").modal("show");

            $("input[name=tahun]").val(response.tahun);
            $("input[name=skpd]").val(response.skpd);
            $("input[name=pns]").val(response.pns);
            $("input[name=sasaran]").val(response.uraian_sasaran);
            $("input[name=indikator]").val(response.uraian_indikator);
            $("input[name=target]").val(response.target);
            $("input[name=realisasi_output]").val(response.target_realisasi);
            $("input[name=program_kegiatan]").val(response.program_kegiatan);
            $("input[name=pagu]").val(IDR(response.pagu).format(true));
            $("input[name=pagu_realisasi]").val(IDR(response.pagu_realisasi).format(true));
            $("input[name=id_indikator]").val(id_indikator);
            $("input[name=id_program_kegiatan]").val(id_program_kegiatan);

            get_data_sumber_anggaran(response.sumber_anggaran_id);
        },
        complete:function(){
            loading_stop();
        }
    });
}

function get_data_sumber_anggaran(id_sumber_anggaran_selected){
    $("select[name=sumber_anggaran]").html("<option value=''>-- Pilih Sumber Anggaran --</option>");
    $.ajax({
        url: base_url+'program_kegiatan/request/get_data_sumber_anggaran',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            let html = "";
            $.each(response,function(index,value){
                let selected = "";
                if(id_sumber_anggaran_selected == value.id_sumber_anggaran){
                    selected = "selected";
                }
                html += "<option "+selected+" value='"+value.id_encrypt+"'>"+value.nama_sumber_anggaran+"</option>";
            });
            $("select[name=sumber_anggaran]").html(html);
        },
        complete:function(){
            loading_stop();
        }
    });
}

</script>