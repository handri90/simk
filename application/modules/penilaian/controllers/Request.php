<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->model('presensi_model');
		$this->load->model('penilaian_anggaran_struktural_model');
		$this->load->model('penilaian_indikator_sasaran_model');
		$this->load->model('program_kegiatan/program_kegiatan_model','program_kegiatan_model');
		$this->load->model('program_kegiatan/program_kegiatan_model','master_sumber_anggaran_model');
		$this->load->model('indikator/indikator_model','indikator_model');
		$this->load->model('sasaran/sasaran_model','sasaran_model');
    }
    
    public function get_data_penilaian(){
        $tahun = $this->iget("tahun");
        $bulan = $this->iget("bulan");
        $skpd = decrypt_data($this->iget("skpd"));
        $pns = decrypt_data($this->iget("pns"));

        $data_penilaian = $this->penilaian_anggaran_struktural_model->get_data_penilaian($skpd,$pns,$tahun,$bulan);

        $templist = array();
        foreach($data_penilaian as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            
            $target = 0;
            if($bulan == "1"){
                $target = $row->m_1;
            }else if($bulan == '2'){
                $target = $row->m_1 + $row->m_2;
            }else if($bulan == '3'){
                $target = $row->m_1 + $row->m_2 + $row->m_3;
            }else if($bulan == '4'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4;
            }else if($bulan == '5'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5;
            }else if($bulan == '6'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6;
            }else if($bulan == '7'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6 + $row->m_7;
            }else if($bulan == '8'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6 + $row->m_7 + $row->m_8;
            }else if($bulan == '9'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6 + $row->m_7 + $row->m_8 + $row->m_9;
            }else if($bulan == '10'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6 + $row->m_7 + $row->m_8 + $row->m_9 + $row->m_10;
            }else if($bulan == '1'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6 + $row->m_7 + $row->m_8 + $row->m_9 + $row->m_10 + $row->m_11;
            }else if($bulan == '12'){
                $target = $row->m_1 + $row->m_2 + $row->m_3 + $row->m_4 + $row->m_5 + $row->m_6 + $row->m_7 + $row->m_8 + $row->m_9 + $row->m_10 + $row->m_11 + $row->m_12;
            }

            $data_realisasi_indikator = $this->penilaian_indikator_sasaran_model->get(
                array(
                    "where"=>array(
                        "indikator_sasaran_id"=>$row->id_indikator_sasaran,
                        "n_bulan"=>$bulan,
                        "type_input"=>"0"
                    )
                ),"row"
            );
            
            $data_realisasi_anggaran = $this->penilaian_anggaran_struktural_model->get(
                array(
                    "where"=>array(
                        "program_kegiatan_struktural_id"=>$row->id_program_kegiatan_struktural,
                        "n_bulan"=>$bulan,
                        "type_input"=>"0"
                    )
                ),"row"
            );

            if($data_realisasi_indikator){
                $persentase_target_realisasi = ($data_realisasi_indikator->nilai_realisasi/$target)*100;
            }else{
                $persentase_target_realisasi = "";
            }
            
            if($data_realisasi_anggaran){
                $persentase_realisasi_anggaran = ($data_realisasi_anggaran->jumlah_anggaran_realisasi/$row->jumlah_anggaran)*100;
            }else{
                $persentase_realisasi_anggaran = "";
            }
            
            $templist[$key]['target'] = $target;
            $templist[$key]['realisasi'] = ($data_realisasi_indikator?$data_realisasi_indikator->nilai_realisasi:0);
            $templist[$key]['realisasi_anggaran'] = ($data_realisasi_anggaran?$data_realisasi_anggaran->jumlah_anggaran_realisasi:0);
            $templist[$key]['persentase_target_realisasi'] = round($persentase_target_realisasi,2);
            $templist[$key]['persentase_realisasi_anggaran'] = round($persentase_realisasi_anggaran,2);
            $templist[$key]['id_indikator_encrypt'] = encrypt_data($row->id_indikator_sasaran);
            $templist[$key]['id_program_kegiatan_encrypt'] = encrypt_data($row->id_program_kegiatan_struktural);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_pns(){
        $skpd = decrypt_data($this->iget("skpd"));
        
        $data_pns = $this->presensi_model->get_data_pns($skpd);
        
        $templist = array();
        foreach($data_pns as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['pns_nip_encrypt'] = encrypt_data($row->PNS_PNSNIP);
        }
        
        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function get_data_penilaian_by_id(){
        $id_indikator = decrypt_data($this->iget("id_indikator"));
        $id_program_kegiatan = decrypt_data($this->iget("id_program_kegiatan"));
        $skpd = decrypt_data($this->iget("skpd"));
        $pns = decrypt_data($this->iget("pns"));
        $tahun = $this->iget("tahun");
        $bulan = $this->iget("bulan");

        $data_indikator = $this->indikator_model->get_by($id_indikator);

        $data_sasaran = $this->sasaran_model->get_data_sasaran($data_indikator->sasaran_id);

        $target = 0;
        if($bulan == "1"){
            $target = $data_indikator->m_1;
        }else if($bulan == '2'){
            $target = $data_indikator->m_1 + $data_indikator->m_2;
        }else if($bulan == '3'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3;
        }else if($bulan == '4'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4;
        }else if($bulan == '5'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5;
        }else if($bulan == '6'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5 + $data_indikator->m_6;
        }else if($bulan == '7'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5 + $data_indikator->m_6 + $data_indikator->m_7;
        }else if($bulan == '8'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5 + $data_indikator->m_6 + $data_indikator->m_7 + $data_indikator->m_8;
        }else if($bulan == '9'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5 + $data_indikator->m_6 + $data_indikator->m_7 + $data_indikator->m_8 + $data_indikator->m_9;
        }else if($bulan == '10'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5 + $data_indikator->m_6 + $data_indikator->m_7 + $data_indikator->m_8 + $data_indikator->m_9 + $data_indikator->m_10;
        }else if($bulan == '1'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5 + $data_indikator->m_6 + $data_indikator->m_7 + $data_indikator->m_8 + $data_indikator->m_9 + $data_indikator->m_10 + $data_indikator->m_11;
        }else if($bulan == '12'){
            $target = $data_indikator->m_1 + $data_indikator->m_2 + $data_indikator->m_3 + $data_indikator->m_4 + $data_indikator->m_5 + $data_indikator->m_6 + $data_indikator->m_7 + $data_indikator->m_8 + $data_indikator->m_9 + $data_indikator->m_10 + $data_indikator->m_11 + $data_indikator->m_12;
        }

        $data_realisasi_indikator = $this->penilaian_indikator_sasaran_model->get(
            array(
                "where"=>array(
                    "indikator_sasaran_id"=>$id_indikator,
                    "n_bulan"=>$bulan,
                    "type_input"=>"0"
                )
            ),"row"
        );
        
        $data_realisasi_anggaran = $this->penilaian_anggaran_struktural_model->get(
            array(
                "where"=>array(
                    "program_kegiatan_struktural_id"=>$id_program_kegiatan,
                    "n_bulan"=>$bulan,
                    "type_input"=>"0"
                )
            ),"row"
        );

        $data_program_kegiatan = $this->program_kegiatan_model->get_data_program_kegiatan_by_id($id_program_kegiatan);
        
        $data = array(
            "tahun"=>$data_sasaran->tahun,
            "skpd"=>$data_sasaran->skpd,
            "pns"=>$data_sasaran->nama_pns,
            "uraian_sasaran"=>$data_sasaran->uraian_sasaran,
            "uraian_indikator"=>$data_indikator->uraian_indikator,
            "target"=>$target,
            "target_realisasi"=>$data_realisasi_indikator?$data_realisasi_indikator->nilai_realisasi:0,
            "program_kegiatan"=>$data_program_kegiatan->uraian_program_kegiatan,
            "pagu"=>$data_program_kegiatan->jumlah_anggaran,
            "pagu_realisasi"=>$data_realisasi_anggaran?$data_realisasi_anggaran->jumlah_anggaran_realisasi:0,
            "sumber_anggaran_id"=>$data_program_kegiatan->sumber_anggaran_id,
        );

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
