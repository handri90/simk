<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penilaian extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('presensi_model');
		$this->load->model('penilaian_indikator_sasaran_model');
		$this->load->model('penilaian_anggaran_struktural_model');
        $this->load->model('master_periode/master_periode_model','master_periode_model');
	}

	public function struktural()
	{
        $list_tahun = $this->master_periode_model->get(
            array(
                "where"=>array(
                    "periode_is_active"=>"1"
                )
            ),"row"
        );

        $year_now = date("Y");

        $tahun_arr = array();

        for($i = $list_tahun->tahun_awal;$i<=$list_tahun->tahun_akhir;$i++){
            if($i<=$year_now){
                array_push($tahun_arr,$i);
            }
        }

        $data['show_option'] = false;
        if(!$tahun_arr){
            $data['show_option'] = true;
        }
        
        $data['list_bulan'] = array("1","2","3","4","5","6","7","8","9","10","11","12");

        $data['list_tahun'] = $tahun_arr;

        $data['list_skpd'] = $this->presensi_model->get_data_skpd();

        $data['breadcrumb'] = [['link'=>false,'content'=>'Penilaian','is_active'=>true]];
        $this->execute('index',$data);
    }

    public function act_penilaian(){
        $id_indikator = decrypt_data($this->ipost("id_indikator"));
        $id_program_kegiatan = decrypt_data($this->ipost("id_program_kegiatan"));
        $bulan = $this->ipost("bulan");
        $realisasi_output = $this->ipost("realisasi_output");
        $pagu_realisasi = replace_dot($this->ipost("pagu_realisasi"));

        $cek_data_penilaian_indikator = $this->penilaian_indikator_sasaran_model->get(
            array(
                "where"=>array(
                    "indikator_sasaran_id"=>$id_indikator,
                    "n_bulan"=>$bulan,
                    "type_input"=>"0",
                )
            ),"row"
        );

        if($cek_data_penilaian_indikator){
            $data = array(
                'nilai_realisasi'=>$realisasi_output,
                'updated_at'=>$this->datetime()
            );
    
            $status = $this->penilaian_indikator_sasaran_model->edit($cek_data_penilaian_indikator->id_penilaian_indikator_sasaran,$data);
            
        }else{
            $data = array(
                "indikator_sasaran_id"=>$id_indikator,
                "n_bulan"=>$bulan,
                "type_input"=>"0",
                'nilai_realisasi'=>$realisasi_output,
                'created_at'=>$this->datetime()
            );
    
            $status = $this->penilaian_indikator_sasaran_model->save($data);
        }
        
        $cek_data_penilaian_anggaran = $this->penilaian_anggaran_struktural_model->get(
            array(
                "where"=>array(
                    "program_kegiatan_struktural_id"=>$id_program_kegiatan,
                    "n_bulan"=>$bulan,
                    "type_input"=>"0",
                )
            ),"row"
        );

        if($cek_data_penilaian_anggaran){
            $data = array(
                'jumlah_anggaran_realisasi'=>$pagu_realisasi,
                'updated_at'=>$this->datetime()
            );
    
            $status = $this->penilaian_anggaran_struktural_model->edit($cek_data_penilaian_anggaran->id_penilaian_anggaran_struktural,$data);
        }else{
            $data = array(
                "program_kegiatan_struktural_id"=>$id_program_kegiatan,
                "n_bulan"=>$bulan,
                "type_input"=>"0",
                'jumlah_anggaran_realisasi'=>$pagu_realisasi,
                'created_at'=>$this->datetime()
            );
    
            $status = $this->penilaian_anggaran_struktural_model->save($data);
        }
        

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
