<?php

class Master_periode_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="master_periode";
        $this->primary_id="id_periode";
    }
}