<?php

class Program_kegiatan_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="program_kegiatan_struktural";
        $this->primary_id="id_program_kegiatan_struktural";

        $this->dbkinerja = $this->load->database($this->config->item('kinerja'),true);
        
        $this->simk = $this->db->database;

        $this->kinerja = $this->config->item('kinerja');
        $this->presensi = $this->config->item('presensi');

    }

    public function get_data_program_kegiatan($indikator){
        $this->db->select("{$this->simk}.program_kegiatan_struktural.*,{$this->simk}.master_sumber_anggaran.nama_sumber_anggaran");
        // $this->db->join("{$this->kinerja}.pekerjaan", "{$this->kinerja}.pekerjaan.id={$this->simk}.program_kegiatan_struktural.pekerjaan_id");
        $this->db->join("{$this->simk}.master_sumber_anggaran", "{$this->simk}.master_sumber_anggaran.id_sumber_anggaran={$this->simk}.program_kegiatan_struktural.sumber_anggaran_id");
        $this->db->where("{$this->simk}.program_kegiatan_struktural.indikator_sasaran_id",$indikator);
        $this->db->where("{$this->simk}.program_kegiatan_struktural.deleted_at",NULL);
        $this->db->where("{$this->simk}.program_kegiatan_struktural.type_input","0");
        return $this->db->get("{$this->simk}.program_kegiatan_struktural")->result();
    }

    public function get_data_program_kegiatan_by_id($id_program_kegiatan){
        $this->db->select("{$this->simk}.program_kegiatan_struktural.*,{$this->simk}.indikator_sasaran.uraian_indikator,{$this->presensi}.unor.NM_UNOR AS skpd,{$this->presensi}.pns.PNS_PNSNAM AS nama_pns,{$this->simk}.sasaran.uraian_sasaran,{$this->simk}.sasaran.tahun");
        $this->db->join("{$this->simk}.indikator_sasaran", "{$this->simk}.indikator_sasaran.id_indikator_sasaran={$this->simk}.program_kegiatan_struktural.indikator_sasaran_id");
        $this->db->join("{$this->simk}.sasaran", "{$this->simk}.sasaran.id_sasaran={$this->simk}.indikator_sasaran.sasaran_id");
        $this->db->join("{$this->presensi}.pns", "{$this->presensi}.pns.PNS_PNSNIP={$this->simk}.sasaran.pns_nip");
        $this->db->join("{$this->presensi}.unor", "{$this->presensi}.unor.KD_UNOR={$this->simk}.sasaran.unor");
        $this->db->where("{$this->simk}.program_kegiatan_struktural.id_program_kegiatan_struktural",$id_program_kegiatan);
        $this->db->where("{$this->simk}.program_kegiatan_struktural.deleted_at",NULL);
        $this->db->where("{$this->simk}.indikator_sasaran.deleted_at",NULL);
        $this->db->where("{$this->simk}.sasaran.deleted_at",NULL);
        return $this->db->get("{$this->simk}.program_kegiatan_struktural")->row();
    }

    // public function get_data_pekerjaan($skpd,$pns){
    //     return $this->dbkinerja->query(
    //         "
    //         SELECT * 
    //         FROM pekerjaan 
    //         WHERE PNS_UNOR = ".$skpd."
    //         AND id_jabatan = (
    //             SELECT presensi.pns.PNS_JABSTR 
    //             FROM presensi.pns 
    //             WHERE presensi.pns.PNS_PNSNIP=".$pns."
    //         )
    //         "
    //     )->result();
    // }
}