<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->model('presensi_model');
		$this->load->model('program_kegiatan_model');
		$this->load->model('master_sumber_anggaran_model');
		$this->load->model('indikator/indikator_model','indikator_model');
		$this->load->model('sasaran/sasaran_model','sasaran_model');
    }
    
    public function get_data_program_kegiatan(){
        $indikator = decrypt_data($this->iget("indikator"));

        $data_program_kegiatan = $this->program_kegiatan_model->get_data_program_kegiatan($indikator);

        $templist = array();
        foreach($data_program_kegiatan as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_program_kegiatan_struktural);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

	public function get_data_sasaran(){
        $tahun = $this->iget("tahun");
        $pns = decrypt_data($this->iget("pns"));
        $skpd = decrypt_data($this->iget("skpd"));

        $data_sasaran = $this->sasaran_model->get(
            array(
                "where"=>array(
                    "unor"=>$skpd,
                    "tahun"=>$tahun,
                    "pns_nip"=>$pns
                )
            )
        );

        $templist = array();
        foreach($data_sasaran as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_sasaran);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_pns(){
        $skpd = decrypt_data($this->iget("skpd"));

        $data_pns = $this->presensi_model->get_data_pns($skpd);

        $templist = array();
        foreach($data_pns as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['pns_nip_encrypt'] = encrypt_data($row->PNS_PNSNIP);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_indikator(){
        $sasaran = decrypt_data($this->iget("sasaran"));

        $data_indikator = $this->indikator_model->get(
            array(
                "where"=>array(
                    "sasaran_id"=>$sasaran
                )
            )
        );

        $templist = array();
        foreach($data_indikator as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_indikator_sasaran);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_pekerjaan(){
        $skpd = decrypt_data($this->iget("skpd"));
        $pns = decrypt_data($this->iget("pns"));

        $data_pekerjaan = $this->program_kegiatan_model->get_data_pekerjaan($skpd,$pns);

        $templist = array();
        foreach($data_pekerjaan as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function get_data_sumber_anggaran(){
        
        $data_sumber_anggaran = $this->master_sumber_anggaran_model->get();
        
        $templist = array();
        foreach($data_sumber_anggaran as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_sumber_anggaran);
        }
        
        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function get_data_program_kegiatan_by_id(){
        $id_program_kegiatan = decrypt_data($this->iget("id_program_kegiatan"));

        $data = $this->program_kegiatan_model->get_data_program_kegiatan_by_id($id_program_kegiatan);
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
