<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program_kegiatan extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('presensi_model');
		$this->load->model('program_kegiatan_model');
        $this->load->model('master_periode/master_periode_model','master_periode_model');
	}

	public function struktural()
	{
        $list_tahun = $this->master_periode_model->get(
            array(
                "where"=>array(
                    "periode_is_active"=>"1"
                )
            ),"row"
        );

        $year_now = date("Y");

        $tahun_arr = array();

        for($i = $list_tahun->tahun_awal;$i<=$list_tahun->tahun_akhir;$i++){
            if($i<=$year_now){
                array_push($tahun_arr,$i);
            }
        }

        $data['show_option'] = false;
        if(!$tahun_arr){
            $data['show_option'] = true;
        }

        $data['list_tahun'] = $tahun_arr;

        $data['list_skpd'] = $this->presensi_model->get_data_skpd();

        $data['breadcrumb'] = [['link'=>false,'content'=>'Program Kegiatan Struktural','is_active'=>true]];
        $this->execute('index',$data);
    }

    public function act_program_kegiatan(){
        $program_kegiatan = $this->ipost("program_kegiatan");
        $jumlah_anggaran = replace_dot($this->ipost("jumlah_anggaran"));
        $sumber_anggaran = decrypt_data($this->ipost("sumber_anggaran"));
        $id_program_kegiatan_struktural = decrypt_data($this->ipost("id_program_kegiatan_struktural"));
        $indikator = decrypt_data($this->ipost("indikator"));
        
        if($id_program_kegiatan_struktural){
            $data = array(
                'indikator_sasaran_id'=>$indikator,
                'uraian_program_kegiatan'=>$program_kegiatan,
                'jumlah_anggaran'=>$jumlah_anggaran,
                'sumber_anggaran_id'=>$sumber_anggaran,
                'updated_at'=>$this->datetime()
            );
    
            $status = $this->program_kegiatan_model->edit($id_program_kegiatan_struktural,$data);
        }else{
            $data = array(
                'indikator_sasaran_id'=>$indikator,
                'uraian_program_kegiatan'=>$program_kegiatan,
                'jumlah_anggaran'=>$jumlah_anggaran,
                'sumber_anggaran_id'=>$sumber_anggaran,
                "type_input"=>"0",
                'created_at'=>$this->datetime()
            );
    
            $status = $this->program_kegiatan_model->save($data);
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function delete_program_kegiatan(){
        $id_program_kegiatan_struktural = $this->iget('id_program_kegiatan_struktural');
        $data_master = $this->program_kegiatan_model->get_by(decrypt_data($id_program_kegiatan_struktural));

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->program_kegiatan_model->remove(decrypt_data($id_program_kegiatan_struktural));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
