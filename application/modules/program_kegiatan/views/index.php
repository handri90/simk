<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tahun : </label>
                        <select class="form-control select-search" name="tahun">
                            <?php
                            echo ($show_option?"<option value=''>-- Pilih Tahun --</option>":"");
                            foreach($list_tahun as $key=>$row){
                                $selected = "";
                                if($row == date('Y')){
                                    $selected = "selected";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $row; ?>"><?php echo $row; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>SKPD : </label>
                        <select class="form-control select-search" name="skpd" onChange="get_data_pns()">
                            <option value="">-- Pilih SKPD --</option>
                            <?php
                            foreach($list_skpd as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->KD_UNOR); ?>"><?php echo $row->NM_UNOR; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>PNS : </label>
                        <select class="form-control select-search" name="pns" onChange="get_data_sasaran()">
                            <option value="">-- Pilih PNS --</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Sasaran : </label>
                        <select class="form-control select-search" name="sasaran" onChange="get_data_indikator()">
                            <option value="">-- Pilih Sasaran --</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Indikator : </label>
                        <select class="form-control select-search" name="indikator" onChange="get_data_program_kegiatan()">
                            <option value="">-- Pilih Indikator --</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="box-top text-right">
                <a href="#tambahProgramKegiatan" class="btn btn-info">Tambah Program/Kegiatan</a>
            </div>
        </div>
        <table id="datatableProgramKegiatan" class="table datatable-save-state table-bordered table-striped table-xs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Program Kegiatan</th>
                    <th>Jumlah Anggaran</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modal_form_program_kegiatan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title"><span class="header_name_modal"></span> Program Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <fieldset class="mb-3">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Tahun <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="tahun">
                            <input type="hidden" name="id_program_kegiatan_struktural" />
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">SKPD <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="skpd">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">PNS <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="pns">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Sasaran <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="sasaran">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Indikator <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" readonly class="form-control" name="indikator">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Program Kegiatan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="program_kegiatan" required placeholder="Program Kegiatan">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Jumlah Anggaran <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control input-currency" name="jumlah_anggaran" required placeholder="Jumlah Anggaran">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Sumber Anggaran <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="sumber_anggaran">
                                <option value="">-- Pilih Sumber Anggaran --</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn bg-primary" onClick="btn_act_program_kegiatan()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
$(".box-top").hide();

const IDR = value => currency(
    value, { 
        symbol: "",
        precision: 0,
        separator: "." 
});

var cleave = new Cleave('.input-currency', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.',
});

let datatableProgramKegiatan = $("#datatableProgramKegiatan").DataTable();

function get_data_program_kegiatan(){
    $(".box-top").hide();

    let indikator = $("select[name=indikator]").val();

    datatableProgramKegiatan.clear().draw();
    if(indikator){
        $(".box-top").show();

        $.ajax({
            url: base_url+'program_kegiatan/request/get_data_program_kegiatan',
            data:{indikator:indikator},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let no = 1;
                $.each(response,function(index,value){
                    datatableProgramKegiatan.row.add([
                        no,
                        value.uraian_program_kegiatan,
                        IDR(value.jumlah_anggaran).format(true),
                        value.nama_sumber_anggaran,
                        "<a href='#' onclick=\"open_form('"+value.id_encrypt+"')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function get_data_indikator(){
    let sasaran = $("select[name=sasaran]").val();

    datatableProgramKegiatan.clear().draw();
    $("select[name=indikator]").html("<option value=''>-- Pilih Indikator --</option>");
    if(sasaran){
        $.ajax({
            url: base_url+'program_kegiatan/request/get_data_indikator',
            data:{sasaran:sasaran},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let html = "";
                html += "<option value=''>-- Pilih Indikator --</option>";
                $.each(response,function(index,value){
                    html += "<option value='"+value.id_encrypt+"'>"+value.uraian_indikator+"</option>";
                });
                $("select[name=indikator]").html(html);
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function get_data_sasaran(){

    let tahun = $("select[name=tahun]").val();
    let skpd = $("select[name=skpd]").val();
    let pns = $("select[name=pns]").val();

    datatableProgramKegiatan.clear().draw();
    if(tahun && skpd && pns){
        $("select[name=sasaran]").html("<option value=''>-- Pilih Sasaran --</option>");
        $.ajax({
            url: base_url+'program_kegiatan/request/get_data_sasaran',
            data:{tahun:tahun,skpd:skpd,pns:pns},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let html = "";
                html += "<option value=''>-- Pilih Sasaran --</option>";
                $.each(response,function(index,value){
                    html += "<option value='"+value.id_encrypt+"'>"+value.uraian_sasaran+"</option>";
                });
                $("select[name=sasaran]").html(html);
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

function confirm_delete(id_program_kegiatan_struktural){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'program_kegiatan/delete_program_kegiatan',
                data : {id_program_kegiatan_struktural:id_program_kegiatan_struktural},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    get_data_program_kegiatan();
                    if(response){
                        swalInit(
                            'Berhasil',
                            'Data berhasil dihapus',
                            'success'
                        );
                    }else{
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_program_kegiatan();
                }
            });
        }
    });
}

function get_data_pns(){
    let skpd = $("select[name=skpd]").val();
    datatableProgramKegiatan.clear().draw();
    $("select[name=pns]").html("<option value=''>-- Pilih PNS --</option>");
    if(skpd){
        $.ajax({
            url: base_url+'program_kegiatan/request/get_data_pns',
            data:{skpd:skpd},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let html = "";
                html += "<option value=''>-- Pilih PNS --</option>";
                $.each(response,function(index,value){
                    html += "<option value='"+value.pns_nip_encrypt+"'>"+value.PNS_PNSNAM+"</option>";
                });
                $("select[name=pns]").html(html);
            },
            complete:function(){
                loading_stop();
            }
        });
    }
}

$("a[href$='#tambahProgramKegiatan']").on("click",function(){
    let tahun = $("select[name=tahun] option:selected").html();
    let skpd = $("select[name=skpd] option:selected").html();
    let pns = $("select[name=pns] option:selected").html();
    let sasaran = $("select[name=sasaran] option:selected").html();
    let indikator = $("select[name=indikator] option:selected").html();

    $("#modal_form_program_kegiatan").modal("show");

    $("input[name=tahun]").val(tahun);
    $("input[name=skpd]").val(skpd);
    $("input[name=pns]").val(pns);
    $("input[name=sasaran]").val(sasaran);
    $("input[name=indikator]").val(indikator);

    $(".header_name_modal").html("Tambah");

    get_data_sumber_anggaran();
    // get_data_pekerjaan();
});

function btn_act_program_kegiatan(){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    let id_program_kegiatan_struktural = $("input[name=id_program_kegiatan_struktural]").val();
    let program_kegiatan = $("input[name=program_kegiatan]").val();
    let jumlah_anggaran = $("input[name=jumlah_anggaran]").val();
    let sumber_anggaran = $("select[name=sumber_anggaran]").val();
    let indikator = $("select[name=indikator]").val();

    if(program_kegiatan && jumlah_anggaran && sumber_anggaran){
        if(indikator){
            $.ajax({
                url: base_url+'program_kegiatan/act_program_kegiatan',
                data:{program_kegiatan:program_kegiatan,jumlah_anggaran:jumlah_anggaran,sumber_anggaran:sumber_anggaran,id_program_kegiatan_struktural:id_program_kegiatan_struktural,indikator:indikator},
                type: 'POST',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    $("#modal_form_program_kegiatan").modal("toggle");

                    $("input[name=tahun]").val("");
                    $("input[name=skpd]").val("");
                    $("input[name=pns]").val("");
                    $("input[name=sasaran]").val("");
                    $("input[name=indikator]").val("");
                    $("input[name=program_kegiatan]").val("");
                    $("input[name=jumlah_anggaran]").val("");
                    $("input[name=sumber_anggaran]").val("");
                    $("input[name=id_program_kegiatan_struktural]").val("");
                    $("select[name=sumber_anggaran]").html("<option value=''>-- Pilih Sumber Anggaran --</option>");
                    get_data_program_kegiatan();

                    if(id_program_kegiatan_struktural){
                        swalInit(
                            'Berhasil',
                            'Data berhasil diubah!',
                            'success'
                        );
                    }else{
                        swalInit(
                            'Berhasil',
                            'Data baru berhasil ditambahkan!',
                            'success'
                        );
                    }
                },
                complete:function(){
                    loading_stop();
                }
            });
        }else{
            swalInit(
                'Gagal',
                'Indikator tidak boleh kosong!',
                'error'
            );
        }
    }else{
        swalInit(
            'Gagal',
            'Field tidak boleh kosong!',
            'error'
        );
    }
}

function open_form(id_program_kegiatan){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    $.ajax({
        url: base_url+'program_kegiatan/request/get_data_program_kegiatan_by_id',
        data:{id_program_kegiatan:id_program_kegiatan},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $("#modal_form_program_kegiatan").modal("show");

            $("input[name=tahun]").val(response.tahun);
            $("input[name=skpd]").val(response.skpd);
            $("input[name=pns]").val(response.nama_pns);
            $("input[name=sasaran]").val(response.uraian_sasaran);
            $("input[name=indikator]").val(response.uraian_indikator);
            $("input[name=program_kegiatan]").val(response.uraian_program_kegiatan);
            $("input[name=jumlah_anggaran]").val(IDR(response.jumlah_anggaran).format(true));
            $("input[name=id_program_kegiatan_struktural]").val(id_program_kegiatan);

            $(".header_name_modal").html("Ubah");

            get_data_sumber_anggaran(response.sumber_anggaran_id);
        },
        complete:function(){
            loading_stop();
        }
    });
}

// function get_data_pekerjaan(id_pekerjaan_selected){
//     let skpd = $("select[name=skpd]").val();
//     let pns = $("select[name=pns]").val();

//     $("select[name=pekerjaan]").html("<option value=''>-- Pilih Pekerjaan --</option>");
//     $.ajax({
//         url: base_url+'program_kegiatan/request/get_data_pekerjaan',
//         data:{skpd:skpd,pns:pns},
//         type: 'GET',
//         beforeSend: function(){
//             loading_start();
//         },
//         success: function(response){
//             let html = "";
//             html += "<option value=''>-- Pilih Pekerjaan --</option>";
//             $.each(response,function(index,value){
//                 let selected = "";
//                 if(id_pekerjaan_selected == value.id){
//                     selected = "selected";
//                 }
//                 html += "<option "+selected+" value='"+value.id_encrypt+"'>"+value.nama_pekerjaan+"</option>";
//             });
//             $("select[name=pekerjaan]").html(html);
//         },
//         complete:function(){
//             loading_stop();
//         }
//     });
// }

function get_data_sumber_anggaran(id_sumber_anggaran_selected){
    $("select[name=sumber_anggaran]").html("<option value=''>-- Pilih Sumber Anggaran --</option>");
    $.ajax({
        url: base_url+'program_kegiatan/request/get_data_sumber_anggaran',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            let html = "";
            html += "<option value=''>-- Pilih Sumber Anggaran --</option>";
            $.each(response,function(index,value){
                let selected = "";
                if(id_sumber_anggaran_selected == value.id_sumber_anggaran){
                    selected = "selected";
                }
                html += "<option "+selected+" value='"+value.id_encrypt+"'>"+value.nama_sumber_anggaran+"</option>";
            });
            $("select[name=sumber_anggaran]").html(html);
        },
        complete:function(){
            loading_stop();
        }
    });
}

</script>