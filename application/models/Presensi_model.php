<?php

class Presensi_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->dbpresensi = $this->load->database($this->config->item('presensi'),true);

        $this->presensi = $this->config->item('presensi');

    }

    public function get_data_skpd(){
        $this->dbpresensi->select("{$this->presensi}.unor.*");
        $this->dbpresensi->order_by("{$this->presensi}.unor.NM_UNOR","ASC");
        return $this->dbpresensi->get("{$this->presensi}.unor")->result();
    }

    public function get_data_pns($skpd){
        return $this->dbpresensi->query(
            "SELECT * 
                FROM pns 
            WHERE PNS_UNOR = '".$skpd."' 
            AND PNS_PNSNIP NOT IN (
                SELECT nip 
                FROM pns_ex 
                WHERE unor = '".$skpd."'
                ) 
            AND is_tkd = '0' 
            AND PNS_JABSTR != '9999'
            ORDER BY PNS_JABSTR
        ")->result();
    }
}