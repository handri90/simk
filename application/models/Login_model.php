<?php

class Login_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="users";
        $this->primary_id="id";

        $this->dbkinerja = $this->load->database($this->config->item('kinerja'),true);
        $this->dbpresensi = $this->load->database($this->config->item('presensi'),true);

        $this->kinerja = $this->config->item('kinerja');
        $this->presensi = $this->config->item('presensi');
    }

    public function cek_user_kinerja($username,$password){
        $this->dbkinerja->select("{$this->kinerja}.users.username,{$this->kinerja}.users.first_name,{$this->kinerja}.users.last_name,{$this->presensi}.pns.PNS_PHOTO");
        $this->dbkinerja->join("{$this->presensi}.pns", "{$this->presensi}.pns.PNS_PNSNIP={$this->kinerja}.users.nip", 'left');
        $this->dbkinerja->where("{$this->kinerja}.users.username",$username);
        $this->dbkinerja->where("{$this->kinerja}.users.password",$password);
        return $this->dbkinerja->get("{$this->kinerja}.users")->row();
    }
}