<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
// require APPPATH . 'core/MY_Controller.php';

class Gate_umkm extends REST_Controller{

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('login_model');
    }

    public function userauthentication_get(){
        $username = $this->get("username");
        $password = $this->get("password");

        $get_passw = $this->login_model->get(
            array(
                "fields"=>"password",
                "where"=>array(
                    "username"=>$username
                )
            ),"row"
        );

        if($get_passw){
            if(password_verify($password,$get_passw->password)){
                $user = $this->login_model->get(
                    array(
                        "fields"=>"nama_lengkap,CONCAT('http://192.168.100.15/umkm/assets/foto_user/',foto_user) as foto_user",
                        "where"=>array(
                            "username"=>$username
                        )
                    ),"row"
                );
            }else{
                $user = array();
            }
        }else{
            $user = array();
        }


        if($user){
            $this->response([
                'status' => true,
                'data' => array($user)
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => $user
            ], REST_Controller::HTTP_OK);
        }
    }

}

?>