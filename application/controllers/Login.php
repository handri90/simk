<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('login_model');

    }

	public function index()
	{
        if($this->session->userdata('is_logged_in') == true){
            redirect('dashboard');
        }else{
            $this->act_login();
        }
    }
    
    public function act_login(){
        
    	$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('login');
        }
        else
        {
            $username = $this->input->post('username',true);
            $password = sha1($this->input->post('password'));
            
            $data_user = $this->login_model->cek_user_kinerja($username,$password);
            
            if($data_user){
                $this->session->set_userdata('nama_lengkap',$data_user->first_name." ".$data_user->last_name);
                $this->session->set_userdata('is_logged_in',true);
                redirect('dashboard');
            }else{
                $this->session->set_flashdata('message','username dan password salah');
                redirect('Login');
            }
        }
    }

    public function act_logout(){
        if (isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] === true) {
            
            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            
            // user logout ok
            redirect('Login');
            
        } else {
            
            redirect('Login');
            
        }
    }
}
